﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BZ_11
{
    public partial class Form1 : Form
    {
        private boreiDataContext boreiDataContext1 = new boreiDataContext();
        public Form1()
        {
            InitializeComponent();
            supplierBindingSource.DataSource = boreiDataContext1.Suppliers;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void supplierBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                boreiDataContext1.SubmitChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void runQuery_Click(object sender, EventArgs e)
        {
            var SuppliersQuery = from suppliers in boreiDataContext1.Suppliers
                                 where suppliers.City == cityTextBox.Text
                                 select suppliers;
            supplierBindingSource.DataSource = SuppliersQuery;

        }
    }
}
